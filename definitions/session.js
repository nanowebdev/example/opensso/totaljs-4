'use strict';

const jwt = require(F.path.definitions('jwt'));

const COOKIE = '__session';
const TIMEOUT = '30'; // in minutes
const SESSION = {};

// We register a new middleware `session`, first priority
NEWMIDDLEWARE('session', function($) {

    var cookie = $.req.cookie(COOKIE);
    var ip = $.req.ip.hash().toString();
    var now = Date.now();

    // A simple prevention for session hijacking
    if (cookie) {
        var arr = cookie.split('|');
        if (arr[1] !== ip)
            cookie = null;
    }

    if (!cookie) {
        cookie = U.GUID(15) + '|' + ip;

        // Writes cookie
        $.res.cookie(COOKIE, cookie);
    }

    // Refresh session
    var refresh = new Date(now + TIMEOUT*60000);

    var session = SESSION[cookie];
    if (session) {
        $.req.session = session;
        if($.req.session.refresh !== undefined) {
            if(now > $.req.session.refresh) {
                SESSION[cookie] = $.req.session = {};
            } else {
                $.req.session.refresh = refresh.getTime();
            }
        }
    } else {
        SESSION[cookie] = $.req.session = {};
        $.req.session.refresh = refresh.getTime();
    }
    $.next();
}, null, true);

NEWMIDDLEWARE('authenticate', function($) {
    var cookie = $.req.cookie(COOKIE);
    var session = SESSION[cookie];
    if(session && session.token) {
        // validate the jwt token
        jwt.verify(session.token, function(err, data) {
            if(err) {// if jwt error or expired
                $.controller.redirect('/logout');
                return;
            }
            // if valid
            $.next();
        });
    } else {
        $.controller.redirect('/');
    }
});

ON('logout', function($) {
    var cookie = $.cookie(COOKIE);
    SESSION[cookie] = $.session = {};
});
