# TotalJS 4
This is a simple example to integrate OpenSSO using TotalJS 4.

### Concept
The goals is to make users can login via OpenSSO login page using TotalJS 4.  
So the flow is:

1. User trying to login via opensso
2. Token send back to home page
3. Save the token to session so we can validate and use the token to every page that require the credentials via middleware.
4. When the token has expired, users logout automatically via middleware.
5. Done.

Why we use session?  
Because using session is the simpler way. For production, better to use redis than session.

### Requirement
- NodeJS 14+
- TotalJS 4

### Installation

1. Run npm
```
npm install
```
2. Edit the `views/index.html` at line 10.  
```html
// CHANGE WITH YOUR OPENSSO URL
href="http://localhost:3000/sso/login/56bed89304f84f8ab756a437530024e9"
```

3. Replace `public.key` and `private.key` from your OpenSSO.  
4. Done.


### How to get URL SSO
1. Login to your OpenSSO
2. Go to menu `My SSO`.
3. Add your new SSO.  
![](https://gitlab.com/nanowebdev/example/opensso/totaljs-4/-/raw/master/public/img/tutorial-1.png)
4. Now you have `URL SSO` login page.
![](https://gitlab.com/nanowebdev/example/opensso/totaljs-4/-/raw/master/public/img/tutorial-2.png)
5. Done.

### How to run the Server
```bash
node index.js
```

### Credits
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken).


### Need Help
Just chat with me via Telegram >> [https://t.me/aalfiann](https://t.me/aalfiann).
