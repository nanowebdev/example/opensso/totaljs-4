'use strict';

const jwt = require(F.path.definitions('jwt'));

exports.install = function() {
	ROUTE('GET /', view_index, ['#session']);
	ROUTE('GET /dashboard', view_dashboard, ['#session','#authenticate']);
	ROUTE('GET /logout', processLogout);
};

function view_index() {
	var self = this;
	if (self.query.token) {
		self.req.session.token = self.query.token;
		self.redirect('/dashboard');
	} else {
		self.view('index');
	}
}

function view_dashboard() {
	var self = this;
	var token = self.req.session.token;
	var datatoken = {};
	if(token) {
		datatoken = jwt.decode(token);
	}
	self.view('dashboard', datatoken.payload);
}

function processLogout() {
	var self = this;
	EMIT('logout', self);
	self.redirect('/');
}